<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    public function index()
    {
    	$wishlist = Auth::user()->wishlist()->paginate(20);

        $breads = [
            'index' => 'Главная',
            'user.wishlist' => 'Лист желаний',
        ];

        $user_contests = Auth::user()->contests()->where('contest_user.status', '!=','canceled')->get();
        $user_contests = $user_contests->groupBy('pivot.contest_id');

    	return view('user.wishlist')
            ->withBreads($breads)
            ->withPagetitle('Лист желаний')
            ->with([
                'user_contests' => $user_contests,
                'wishlist' => $wishlist
            ]);
    }

    public function add($id)
    {
    	$contest = \App\Contest::find($id);

    	if(!$contest || (bool)auth()->user()->wishlist()->where('contests.id', $contest->id)->count())
            return 'none';

        auth()->user()->wishlist()->attach($contest);
    }

    public function delete($id)
    {
    	$contest = \App\Contest::find($id);

    	if(!$contest || !(bool)auth()->user()->wishlist()->where('contests.id', $contest->id)->count())
            return 'none';

        auth()->user()->wishlist()->detach($contest);
    }
}
