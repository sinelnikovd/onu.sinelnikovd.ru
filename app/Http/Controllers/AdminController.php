<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use App\Events\NotifiesEvent;
use App\Events\AddScoreEvent;

class AdminController extends Controller
{
  public function contests()
  {

      if($this->g_user_role !='admin' && $this->g_user_role != 'superadmin')
          abort(404);

      $breads = [
          'index' => 'Главная',
          'admin.contests' => 'Призы',
      ];

      $contests = \App\Contest::with('users')->orderBy('created_at', 'desc')->paginate(30);


      return view('admin.contests')->withBreads($breads)->withContests($contests)->withPagetitle('Призы');
  }

  public function contest(Request $request, $id = false)
  {
      if($this->g_user_role != 'admin' && $this->g_user_role != 'superadmin')
          abort(404);

      $contest = $id ? \App\Contest::findOrFail($id) : false;

      $breads = [
          'index' => 'Главная',
          'admin.contests' => 'Призы',
          'admin.contest' => $contest ? $contest->title : 'Новый приз'
      ];

      if($request->isMethod('post'))
      {
          $this->validate($request, [
              'title' => 'required',
              'content' => 'required',
              'photo' => 'image',
              'score' => 'required|integer',
          ], [], [
              'title' => 'Название',
              'content' => 'Описание',
              'photo' => 'Фото',
              'status' => 'Активность',
              'score' => 'Цена'
          ]);

          $data = [];

          if($request->file('photo'))
          {
              $data['photo'] = microtime(1).'.jpg';
              $img = \Image::make($request->file('photo'))->widen(715);
              $img->save('img/contests/'.$data['photo'], 75);
          }

          $data['title'] = $request->get('title');
          $data['content'] = $request->get('content');
          $data['score'] = $request->get('score');
          $data['status'] = $request->get('status');

          if($contest)
              $contest->update($data);
          else
              $cnts = \App\Contest::create($data);

          return redirect()->route('admin.contests')->withMessage('Сохранено!');
      }

      return view('admin.contestform')->withBreads($breads)->withContest($contest)->withPagetitle('Приз');
  }

  public function contestdelete($id)
  {
      if($this->g_user_role != 'admin' && $this->g_user_role != 'superadmin')
          abort(404);

      $contest = \App\Contest::findOrFail($id);
      $contest->delete();
      return back();
  }

  public function buyed()
  {
      if($this->g_user_role != 'admin' && $this->g_user_role != 'superadmin')
          abort(404);

      $breads = [
          'index' => 'Главная',
          'admin.contests' => 'Призы',
          'admin.contest' => 'Призы на обмен'
      ];



      $buyed = \DB::table('contest_user')->join('users', 'users.id', '=', 'contest_user.user_id')->join('contests', 'contests.id', '=', 'contest_user.contest_id')->select(['contest_user.id', 'contest_user.user_id', 'contest_user.status', 'users.name', 'users.email', 'users.agency', 'users.score', 'users.intro', 'contest_user.created_at', 'contest_user.contest_id', 'contests.title', 'contests.photo'])->orderBy('contest_user.created_at', 'desc')->paginate(20);

      return view('admin.buyed')->withBreads($breads)->withBuyed($buyed)->withPagetitle('Призы на обмен');
  }

  public function buyset(Request $request, \App\Accept $accept)
  {
      if(!$accept->controlled($request->get('id')))
          return 'false';

      if($this->g_user_role != 'admin' && $this->g_user_role != 'superadmin')
          return 'false';

      \DB::table('contest_user')->where('id', $request->get('id'))->update(['status' => $request->get('status')]);

      $buy = \DB::table('contest_user')->where('id', $request->get('id'))->first();

      $contest = \App\Contest::find($buy->contest_id);

      \App\Accept::create([
          'status' => $request->get('status'),
          'user_id' => auth()->id(),
          'contest_user_id' => $request->get('id')
      ]);

      if($request->get('status') == 'canceled')
          \App\User::find($buy->user_id)->addscore($contest->score);
  }

  public function export()
  {
      if($this->g_user_role != 'admin' && $this->g_user_role != 'superadmin')
          abort(404);

      $csv = "";

      $buyed = \DB::table('contest_user')->join('users', 'users.id', '=', 'contest_user.user_id')->join('contests', 'contests.id', '=', 'contest_user.contest_id')->select(['contest_user.id', 'contest_user.user_id', 'contest_user.status', 'users.name', 'users.email', 'users.agency', 'users.score', 'users.intro', 'contest_user.created_at', 'contest_user.contest_id', 'contests.title', 'contests.photo'])->orderBy('contest_user.created_at', 'asc')->get();

      $csv .= "id;Название;Кто купил;Agency;Дата;Кол-во баллов;Статус\r\n";

      foreach($buyed as $buy){
        $csv .= "{$buy->id};{$buy->title};{$buy->name};{$buy->agency};";
        $csv .= ($buy->created_at) ? $buy->created_at.';' : ';';
        $csv .= "{$buy->score };{$buy->status};\r\n";
      }

      return response($csv, 200, [
          'Content-Type' => 'application/csv',
          'Content-Disposition' => 'attachment; filename="orders.csv"',
      ]);
  }

}
