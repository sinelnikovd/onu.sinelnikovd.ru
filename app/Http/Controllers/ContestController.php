<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\NotifiesEvent;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;

class ContestController extends Controller
{
    public function index()
    {
        $contests = [];
    	$contests[0] = \App\Contest::where([['status', 1], ['score', '<', '50']])->orderBy('created_at', 'desc')->paginate(2);
    	$contests[1] = \App\Contest::where([['status', 1], ['score', '<', '200'], ['score', '>=', '50']])->orderBy('created_at', 'desc')->paginate(2);
    	$contests[2] = \App\Contest::where([['status', 1], ['score', '<', '500'], ['score', '>=', '200']])->orderBy('created_at', 'desc')->paginate(2);
    	$contests[3] = \App\Contest::where([['status', 1], ['score', '>=', '500']])->orderBy('created_at', 'desc')->paginate(2);


        $max_page_id = 0;
        $max_page_val = $contests[0]->lastPage();
        foreach ($contests as $key=>$val){
            if($max_page_val < $val->lastPage()){
                $max_page_id = $key;
                $max_page_val = $val->lastPage();
            }
        }



        $user_contests = Auth::user()->contests()->where('contest_user.status', '!=','canceled')->get();
        $user_contests = $user_contests->groupBy('pivot.contest_id');

        $breads = [
            'index' => 'Главная',
            'contests' => 'Призы',
        ];


        //return view('contest.index')->withBreads($breads)->withPagetitle('Призы');
    	return view('contest.index')->withBreads($breads)->with([
    	    'contests' => $contests,
            'user_contests' => $user_contests,
            'max_page_id' => $max_page_id,
            ])->withPagetitle('Призы');
    }

    public function buy($id)
    {
    	$contest = \App\Contest::findOrFail($id);

        if(auth()->user()->score < $contest->score || !$contest->status)
            return 'none';

    	auth()->user()->contests()->attach($contest);

        auth()->user()->delscore($contest->score);

        // Уведомление администраторов о покупке товара
        
        /*foreach(\App\User::whereHas('roles', function($q){
            $q->where('title', 'admin');
        })->get() as $user)
        {
            // $user->notifies()->create([
            //     'message' => view('notify.contest')->withContest($contest)->withUser(auth()->user())->render(), //'Покупка "'.$contest->title.'"',
            //     'viewed_at' => 0,
            //     'url' => route('admin.contests')
            // ]);

            $view = view('notify.contest')->withContest($contest)->withUser(auth()->user())->render();

            $notify = $user->notifies()->create([
                'message' => $view,
                'viewed_at' => 0,
                'url' => route('admin.contests')
            ]);

            event(new NotifiesEvent([
                'view' => $view,
                'url' => route('notify', ['id' => $notify->id]),
            ], $user));

            // \Mail::send('emails.contestbuy', ['contest' => $contest, 'user' => $user], function ($m) use ($contest) {
            //     $m->from(env('MAIL_FROM'), config('app.name'));
            //     $m->to($user->email, $user->name)->subject('Обмен на приз '.$contest->title);
            // });
        }*/
    }
}
