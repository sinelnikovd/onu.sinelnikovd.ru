<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $g_user_role;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->g_user_role = Auth::check() ? Auth::user()->role : '';

            view()->share('g_user_role', $this->g_user_role);

            return $next($request);
        });

    }
}
