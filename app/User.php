<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Gerardojbaez\Messenger\Contracts\MessageableInterface;
use Gerardojbaez\Messenger\Traits\Messageable;

class User extends Authenticatable implements MessageableInterface
{
    use Notifiable;
    use Messageable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'uid', 'bday', 'login', 'pass', 'token', 'agency', 'status', 'intro', 'score', 'sex', 'lastname', 'firstname', 'hobby', 'achive', 'nick', 'visited_at', 'email', 'ws_id', 'webinar_aid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function hasRole($name)
    {
        return (bool)($this->role == $name);
    }

    public function getBdayAttribute($v)
    {
        return \Carbon\Carbon::createFromTimestamp($v)->format('d.m.Y'); //toDateTimeString();
    }

    public function setBdayAttribute($v)
    {
        if(empty($v))
            return '';

        if(is_numeric($v))
            $this->attributes['bday'] = $v;
        else
        {
            list($day,$month,$year) = explode('.', $v);
            $this->attributes['bday'] = \Carbon\Carbon::createFromDate($year, $month, $day)->timestamp;
        }
    }

    public function getCoverAttribute($v)
    {
        return \Storage::has('/img/covers/'.$this->id.'.jpg') ? '/img/covers/'.$this->id.'.jpg?'.mt_rand(0,100000) : '/img/covers/0.jpg';
    }

    public function getRoleAttribute()
    {
        return @$this->roles()->first()->title;
    }

    public function getBadgeAttribute()
    {
        $out = new \stdClass;

        switch(true)
        {
            case $this->score < 500 || $this->score == 0:
                $out->icon = '/img/badges/top_new.png';
                $out->text = 'Новичок';
            break;
            case $this->score < 1000:
                $out->icon = '/img/badges/top1.png';
                $out->text = 'Ученик';
            break;
            case $this->score < 2000:
                $out->icon = '/img/badges/top2.png';
                $out->text = 'Студент';
            break;
            default:
                $out->icon = '/img/badges/top3.png';
                $out->text = 'Бакалавр';
        }

        return $out;
    }

    public function getNickAttribute($v)
    {
        return !$v ? 'user-'.$this->id : $v;
    }

    public function getAvatarAttribute()
    {
        return !\Storage::has('img/avatars/'.$this->id.'.jpg') || (auth()->check() && $this->g_user_role == 'moderator' && $this->g_user_role == 'user') ? 'img/avatars/0'.$this->sex.'.jpg' : 'img/avatars/'.$this->id.'.jpg?'.mt_rand(0,100000);
    }

    public function friends()
    {
        return $this->belongsToMany('App\User', 'friends', 'user_id', 'friend_id');
    }

    public function friendlies()
    {
        return $this->belongsToMany('App\User', 'friends', 'friend_id', 'user_id');
    }

    public function isFriend()
    {
        return (bool)auth()->user()->friends()->where('users.id', $this->id)->count();
    }

    public function notifies()
    {
        return $this->hasMany('App\Notify');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function hasAnswer($id)
    {
        return $this->answers()->where('work_id', $id)->whereIn('scored', ['0','1'])->count();
    }

    public function addscore($score)
    {
        $score = $this->score + $score;
        $this->update(['score' => $score]);
    }

    public function delscore($score)
    {
        $score = $this->score - $score;
        $this->update(['score' => $score]);
    }

    public function contests()
    {
        return $this->belongsToMany('App\Contest')->withTimestamps();
    }

    public function wishlist()
    {
        return $this->belongsToMany('App\Contest', 'wishlists', 'user_id', 'contest_id');
    }

    public function histories()
    {
        return $this->hasMany('App\History');
    }

    public function medals()
    {
        return $this->belongsToMany('App\Medal');
    }

    public function mywebinars()
    {
        return $this->belongsToMany('App\Webinar', 'members_webinar', 'user_id', 'webinar_id');
    }
}
