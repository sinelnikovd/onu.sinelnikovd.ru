<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Date\DateFormat;

class Event extends Model
{
    protected $fillable = ['title', 'content', 'photo', 'date', 'gallery'];    

    public function getDateOnAttribute()
    {
        return DateFormat::post($this->date);
    } 

    public function setDateAttribute($v)
    {
    	list($d, $m, $y) = explode('/', $v);
        $this->attributes['date'] = mktime(0,0,0,$m,$d,$y);
    } 

    public function getFdateAttribute()
    {
        return date('d/m/Y', $this->date);
    }

    public function getPhotoAttribute($v)
    {
        // if($this->gallery == '')
            return @$this->gallery[0] ? '/img/events/'.$this->gallery[0] : '';

        // return '/img/events/'.array_shift($this->gallery);
    }
    
    public function getHtmlContentAttribute()
    {
        return $this->content;

        // $bbcode = new \Golonka\BBCode\BBCodeParser;

        // return $bbcode->stripBBCodeTags($bbcode->parse(e($this->content)));
    }

    public function getGalleryAttribute($v)
    {
        return json_decode($v);
    }
}
