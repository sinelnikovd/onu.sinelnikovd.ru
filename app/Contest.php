<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Date\DateFormat;

class Contest extends Model
{
    protected $fillable = ['photo', 'content', 'title', 'photo', 'score', 'status'];

    public function users()
    {
    	return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function buyers()
    {
    	return $this->users()->wherePivot('user_id', auth()->id())->wherePivot('status', '!=','canceled')->count();
    }

    public function getCreatedOnAttribute()
    {
        return DateFormat::post($this->created_at);
    }
}
