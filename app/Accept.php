<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accept extends Model
{
    protected $fillable = ['contest_user_id', 'user_id', 'status'];

    public static function controlled($id)
    {
    	return !(bool)self::where([['user_id', '!=', auth()->id()], ['contest_user_id', '=', $id]])->count();
    }
}
