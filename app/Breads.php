<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Breads extends Model
{
    public static function create($breads = false)
    {
    	return $breads ? view('partials.breads')->withBreads($breads)->render() : '';
    }
}
