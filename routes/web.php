<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::pattern('id', '[0-9]+');


Auth::routes();
Route::any('register', function(){ abort(404); });
Route::get('logout', function(){ return redirect()->to('/'); });


Route::get('notify_webinar', 'CronController@webinar');
Route::get('/', 'PageController@index')->name('index');
Route::get('events', 'EventController@events')->name('events');
Route::get('event/{id}', 'EventController@event')->name('event');
Route::get('lections', 'LectionController@lections')->name('lections');
Route::get('lection/{id}', 'LectionController@lection')->name('lection');
Route::get('about', 'PageController@about')->name('about');
Route::any('forms', 'PageController@forms')->name('forms');
Route::any('email_notify', 'PageController@email_notify')->name('email_notify');
Route::any('check_notify', 'PageController@check_notify')->name('check_notify');
Route::any('notify_birthday', 'CronController@notify_birthday')->name('notify_birthday');
Route::any('check_webinars_stat/{id}', 'CronController@check_webinars_stat')->name('check_webinars_stat');


// Admin
Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function(){
	Route::get('users', 'AdminController@users')->name('admin.users');
	Route::any('user/edit/{id}', 'AdminController@useredit')->name('admin.user.edit');
	Route::get('user/delete/{id}', 'AdminController@userdelete')->name('admin.user.delete');
	Route::any('user/add', 'AdminController@useradd')->name('admin.user.add');
	Route::any('users/import', 'AdminController@import')->name('admin.users.import');
	Route::any('users/export', 'AdminController@export')->name('admin.users.export');
	Route::any('users/notify', 'AdminController@notify')->name('admin.users.notify');
	Route::any('users/addscore/{id?}', 'AdminController@addscore')->name('admin.users.addscore');
	Route::any('users/search', 'AdminController@usersearch')->name('admin.users.search');
	Route::get('webinars', 'AdminController@webinars')->name('admin.webinars');
	Route::any('webinar/{id?}', 'AdminController@webinar')->name('admin.webinar');
	Route::get('webinar/delete/{id}', 'AdminController@webinardelete')->name('admin.webinar.delete');
	Route::get('works', 'AdminController@works')->name('admin.works');
	Route::any('work/{id?}', 'AdminController@work')->name('admin.work');
	Route::get('work/delete/{id}', 'AdminController@workdelete')->name('admin.work.delete');
	Route::get('contests', 'AdminController@contests')->name('admin.contests');
	Route::any('contest/{id?}', 'AdminController@contest')->name('admin.contest');
	Route::any('contests/buyed', 'AdminController@buyed')->name('admin.contests.buyed');
	Route::get('contest/delete/{id}', 'AdminController@contestdelete')->name('admin.contest.delete');
	Route::get('contests/buyed/export', 'AdminController@export')->name('admin.export');
	Route::get('events', 'AdminController@events')->name('admin.events');
	Route::any('event/{id?}', 'AdminController@event')->name('admin.event');
	Route::get('event/delete/{id}', 'AdminController@eventdelete')->name('admin.event.delete');
	Route::get('lections', 'AdminController@lections')->name('admin.lections');
	Route::any('lection/{id?}', 'AdminController@lection')->name('admin.lection');
	Route::get('lection/delete/{id}', 'AdminController@lectiondelete')->name('admin.lection.delete');
	Route::post('contest/buyset', 'AdminController@buyset')->name('admin.buy.set');
	Route::any('about', 'AdminController@about')->name('admin.about');
	Route::post('upload_img', 'PageController@upload_img');
	Route::get('banners', 'AdminController@banners')->name('admin.banners');
	Route::any('banner/{id?}', 'AdminController@banner')->name('admin.banner');
	Route::get('banner/delete/{id}', 'AdminController@bannerdelete')->name('admin.banner.delete');
	Route::get('medals', 'AdminController@medals')->name('admin.medals');
	Route::any('medal/{id?}', 'AdminController@medal')->name('admin.medal');
	Route::get('medal/delete/{id}', 'AdminController@medaldelete')->name('admin.medal.delete');
	Route::get('medal/set', 'AdminController@getMedalSet')->name('admin.medal.set');
	Route::post('medal/set', 'AdminController@postMedalSet');
	Route::post('medal/unset', 'AdminController@postMedalUnset');
	Route::post('medal/search', 'AdminController@search_medal')->name('admin.search_medal');
	Route::post('eventgallery', 'AdminController@eventgallery')->name('admin.eventgallery');
});

// API
Route::group(['prefix' => 'api'], function(){
	Route::get('friends/{id}', 'ApiController@friends')->name('api.friends');
	Route::get('users', 'ApiController@users')->name('api.users');
	Route::any('auth', 'ApiController@auth');
	Route::any('dialogs', 'ApiController@dialogs');
	Route::any('unread_dialogs', 'ApiController@unread_dialogs');
	Route::any('messages', 'ApiController@messages');
	Route::any('message_send', 'ApiController@message_send');
	Route::any('works', 'ApiController@works');
	Route::any('histories', 'ApiController@histories');
});

// Auth
Route::group(['middleware' => 'auth'], function(){
	Route::get('user/edit', 'UserController@edit')->name('user.edit');
	Route::post('user/edit', 'UserController@save');
	Route::any('messenger/{id?}', 'UserController@messenger')->name('messenger');
	Route::any('messenger/{id}/new', 'UserController@messenger_newpost')->name('messenger.new');
	Route::any('messenger/newcount', 'UserController@messenger_newpost_count')->name('messenger.newcount');
	Route::get('notify/{id}', 'NotifyController@index')->name('notify');
	Route::post('work/{id}', 'WorkController@answer');
	Route::post('answer/{id}', 'WorkController@score')->name('answer');
	Route::post('answer/cancel/{id}', 'WorkController@cancel')->name('answer.cancel');
	Route::get('buy/{id}', 'ContestController@buy')->name('buy');
	Route::any('user/{id}', 'UserController@user')->name('user');
	Route::get('webinars', 'WebinarController@webinars')->name('webinars');
	Route::get('webinar/{id}', 'WebinarController@webinar')->name('webinar');
	Route::get('work', 'WorkController@index')->name('works');
	Route::get('work/{id}', 'WorkController@work')->name('work');
	Route::get('heroes', 'HeroesController@heroes')->name('heroes');
	Route::get('contests', 'ContestController@index')->name('contests');
	Route::get('wishlist', 'WishlistController@index')->name('wishlist');
	Route::get('wishlist/add/{id}', 'WishlistController@add')->name('wishlist.add');
	Route::get('wishlist/delete/{id}', 'WishlistController@delete')->name('wishlist.delete');
	Route::any('search_friends/{id}/{all?}', 'UserController@search_friends')->name('search_friends')->where('all', 'all');
	Route::any('clearnotifies', 'UserController@clearnotifies')->name('clearnotifies');
	Route::any('wsid', 'UserController@wsid')->name('wsid');
	Route::get('webinar/stats/{id}', 'WebinarController@stats')->name('webinar.stats');
	Route::get('webinar/stats/{id}/user/{uid}', 'WebinarController@stats_user')->name('webinar.stats.user')->where('uid', '[0-9]+');
});
