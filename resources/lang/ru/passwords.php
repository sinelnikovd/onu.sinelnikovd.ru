<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароли должны быть не менее 6 символов и совпадать',
    'reset' => 'Ваш пароль восстановлен!',
    'sent' => 'На Ваш E-mail отправлена инструкция для восстановления пароля!',
    'token' => 'Не действительный код сброса пароля.',
    'user' => "Пользователь с указанным E-mail не найден.",

];
