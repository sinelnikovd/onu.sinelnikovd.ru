@extends('layouts.app')

@section('content')
    
    <p><a href="{{ route('contests') }}" class="btn btn-primary">Все призы</a></p>

    <div class="ibox">
        <div class="ibox-content">
            @if(!(bool)$wishlist->count())
                <p>Список пуст</p>
            @endif
            @foreach($wishlist as $wish)
                <div class="row">
                    <div class="col-md-3">
                        <img src="/img/contests/{{ $wish->photo }}" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-3">
                        <h3>{{ $wish->title }}</h3>
                        <p>{{ $wish->content }}</p>
                        <p class="h5 text-navy">Цена: {{ $wish->score }} {{ trans_choice('балл|балла|баллов', $wish->score) }}</p>
                    </div>
                    <div class="col-md-3">
                        Вами куплено: @if(isset($user_contests[$wish->id])) {{ count($user_contests[$wish->id]) }} @else 0 @endif
                    </div>
                    <div class="col-md-3">
                        <a href="{{ route('buy', ['id' => $wish->id]) }}" class="btn btn-sm btn-primary btn-buy {{ !$wish->status || !auth()->check() || $wish->score > auth()->user()->score || $g_user_role != 'user' ? 'disabled' : '' }}"><i class="fa fa-shopping-cart"></i> Обменять</a>
                        <a href="{{ route('wishlist.delete', ['id' => $wish->id]) }}" class="btn btn-sm btn-delete btn-danger"><i class="fa fa-delete"></i> Убрать</a>
                    </div>
                </div>
                <hr>
            @endforeach
        </div>
        <div>
            {{  $wishlist->render()  }}
        </div>
    </div>

@endsection

@push('js')
    <script>
        jQuery(function(){
            jQuery('.btn-buy').click(function (e) {
                e.preventDefault();
                var btn = jQuery(this);
                swal({
                    title: "Выбрать этот приз?",
                    text: "",
                    type: "success",
                    showCancelButton: true,
                    confirmButtonColor: "#1ab394",
                    confirmButtonText: "Да, выбрать!",
                    cancelButtonText: "Отменить!",
                    closeOnConfirm: false
                }, function () {
                    jQuery.get(btn.attr('href'), function(data){
                        console.log(data);
                        if(data == 'none')
                            swal("Упс!", "Не достаточно баллов", "warning");
                        else
                            swal({
                                title: "Приз выбран!",
                                text: "Вы обменяли Ваши баллы на приз",
                                type: "success",
                                closeOnConfirm: true
                            }, function(){
                                location.reload();
                            });
                    });
                    
                });
                return false;
            });
            $('.btn-danger').click(function (e) {
                e.preventDefault();
                var btn = jQuery(this);
                swal({
                    title: "Убрать приз",
                    text: "Убрать этот приз из желаний",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#dd3333",
                    confirmButtonText: "Да, убрать!",
                    cancelButtonText: "Отмена!",
                    closeOnConfirm: false
                }, function () {
                    jQuery.get(btn.attr('href'), function(data){
                        if(data == 'none')
                            swal("Упс!", "Не достаточно баллов", "warning");
                        else
                        {
                            btn.closest('.row').remove();
                            swal({
                                title: "Приз удален!",
                                text: "Приз удален из вашего листа желаний",
                                type: "success",
                                closeOnConfirm: true
                            }, function(){
                                location.reload();
                            });
                        }
                    });
                    
                });
                return false;
            });
        });
    </script>
@endpush