<div class="row">
    <div class="col-md-3">
        <img src="/img/contests/{{ $contest->photo }}" alt="" class="img-responsive">
    </div>
    <div class="col-md-3">
        <h3>{{ $contest->title }}</h3>
        <p>{{ $contest->content }}</p>
        <p class="h5 text-navy">Цена: {{ $contest->score }} {{ trans_choice('балл|балла|баллов', $contest->score) }}</p>
    </div>
    <div class="col-md-3">
        Вами куплено: @if(isset($user_contests[$contest->id])) {{ count($user_contests[$contest->id]) }} @else 0 @endif
    </div>

    @if($g_user_role == 'user')
        <div class="col-md-3">
            <a href="{{ route('buy', ['id' => $contest->id]) }}" class="btn btn-sm btn-primary btn-buy {{ !$contest->status || !auth()->check() || $contest->score > auth()->user()->score || $g_user_role != 'user' ? 'disabled' : '' }}"><i class="fa fa-shopping-cart"></i> Обменять</a>
            <a href="{{ route('wishlist.add', ['id' => $contest->id]) }}" class="btn btn-sm btn-warning btn-wish"><i class="fa fa-heart"></i> Хочу это</a>
        </div>
    @endif
</div>
<hr>