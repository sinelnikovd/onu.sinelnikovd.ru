@extends('layouts.app')

@section('content')
    <div class="tabs-container">
        <ul class="nav nav-tabs nav-justified">
            <li class="active"><a data-toggle="tab" href="#tab-1">до 50</a></li>
            <li class=""><a data-toggle="tab" href="#tab-2">от 50 до 200</a></li>
            <li class=""><a data-toggle="tab" href="#tab-3">от 200 до 500</a></li>
            <li class=""><a data-toggle="tab" href="#tab-4">от 500</a></li>
        </ul>
        <div class="tab-content">
            <div id="tab-1" class="tab-pane active">
                <div class="panel-body">
                    @foreach($contests[0] as $contest)
                        @include('contest.item')
                    @endforeach
                </div>
            </div>
            <div id="tab-2" class="tab-pane">
                <div class="panel-body">
                    @foreach($contests[1] as $contest)
                        @include('contest.item')
                    @endforeach
                </div>
            </div>
            <div id="tab-3" class="tab-pane">
                <div class="panel-body">
                    @foreach($contests[2] as $contest)
                        @include('contest.item')
                    @endforeach
                </div>
            </div>
            <div id="tab-4" class="tab-pane">
                <div class="panel-body">
                    @foreach($contests[3] as $contest)
                        @include('contest.item')
                    @endforeach
                </div>
            </div>
        </div>
        <div>
            {{ $contests[$max_page_id]->render() }}
        </div>
    </div>

@endsection

@push('js')
    <script>
        jQuery(function(){
            jQuery('.btn-buy').click(function (e) {
                e.preventDefault();
                var btn = jQuery(this);
                swal({
                    title: "Выбрать этот приз?",
                    text: "",
                    type: "success",
                    showCancelButton: true,
                    confirmButtonColor: "#1ab394",
                    confirmButtonText: "Да, выбрать!",
                    cancelButtonText: "Отменить!",
                    closeOnConfirm: false
                }, function () {
                    jQuery.get(btn.attr('href'), function(data){
                        console.log(data);
                        if(data == 'none')
                            swal("Упс!", "Не достаточно баллов", "warning");
                        else
                            swal({
                                title: "Приз выбран!",
                                text: "Вы обменяли Ваши баллы на приз",
                                type: "success",
                                closeOnConfirm: true
                            }, function(){
                                location.reload();
                            });
                    });
                    
                });
                return false;
            });
            jQuery('.btn-wish').click(function (e) {
                e.preventDefault();
                var btn = jQuery(this);
                swal({
                    title: "Хочу это!",
                    text: "Добавить приз в лист желаний?",
                    type: "success",
                    showCancelButton: true,
                    confirmButtonColor: "#1ab394",
                    confirmButtonText: "Да, добавить!",
                    cancelButtonText: "Отменить!",
                    closeOnConfirm: false
                }, function () {
                    jQuery.get(btn.attr('href'), function(data){
                        if(data == 'none')
                            swal("Приз уже в списке!", "Этот приз уже в вашем листе желаний", "warning");
                        else
                            swal("Приз добавлен!", "Приз добавлен в лист желаний" + data, "success");
                    }).fail(function(data){
                        console.log(data.responseText)
                    });
                    
                });
                return false;
            });
        });
    </script>
@endpush