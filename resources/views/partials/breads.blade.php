<ol class="breadcrumb">
    @foreach($breads as $route => $title)
        @if(!$loop->last)
            <li>
                <a href="{{ route($route) }}">{{ $title }}</a>
            </li>
        @endif
    @endforeach
</ol>