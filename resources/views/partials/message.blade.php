@if($info = Session::get('info') ?: (isset($info) ? $info : false))
	<div class="alert alert-warning" role="alert">
		{!! $info !!}
	</div>
@endif

@if($message = Session::get('message') ?: (isset($message) ? $message : false))
	<div class="alert alert-success" role="alert">
		{!! $message !!}
	</div>
@endif

@if($error = Session::get('error') ?: (isset($error) ? $error : false))
	<div class="alert alert-danger" role="alert">
		{!! $error !!}
	</div>
@endif

@if($errors->count())
	<div class="alert alert-danger" role="alert">
		@foreach($errors->all() as $error)
			<p>{!! $error !!}</p>
		@endforeach
	</div>
@endif