@extends('layouts.app')

@section('content')
    <div class="ibox">
        <div class="ibox-content">
            <div class="text-center article-title">
                <span class="text-muted"><i class="fa fa-clock-o"></i> {{ $page->created_on }}</span>
            </div>
            {!! $page->html_content !!}
        </div>
    </div>
@endsection