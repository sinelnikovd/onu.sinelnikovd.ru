@extends('layouts.app')

@section('content')
    <p>
        <a href="{{ route('admin.contest') }}" class="btn btn-primary">Добавить</a>
        <a href="{{ route('admin.contests.buyed') }}" class="btn btn-primary">Покупки</a>
    </p>
	<div class="ibox">
		<div class="ibox-title">
			<h5>Призы</h5>
		</div>
		<div class="ibox-content">
            <div class="table-responsive">
                <table class="table">
                	<thead>
                        <tr>
                            <th width="70"> </th>
                            <th>Название </th>
                            <th>Цена </th>
                            <th>Куплено </th>
                            <th>Управление</th>
                        </tr>
                    </thead>
                    <tbody>
						@foreach($contests as $contest)
							<tr @if(!(bool)$contest->status) class="bg-muted" @endif>
		                        <td><img src="/img/contests/{{ $contest->photo }}" alt="" class="img-responsive"></td>
		                        <td><h4>{{ $contest->title }}</h4></td>
                                <td>{{ $contest->score }}</td>
		                        <td>{{ $contest->users->count() }}</td>
		                        <td><a href="{{ route('admin.contest', ['id' => $contest->id]) }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a> <a href="{{ route('admin.contest.delete', ['id' => $contest->id]) }}" class="delete-contest btn btn-sm btn-danger"><i class="fa fa-close"></i></a></td>
		                    </tr>
						@endforeach()
                    </tbody>
                </table>
            </div>
		</div>
    </div>

    {{ $contests->links() }}

@endsection

@push('js')
    <script>
        jQuery(function(){
            $('.delete-contest').click(function (e) {
                e.preventDefault();
                var btn = jQuery(this);
                swal({
                    title: "Вы уверены?",
                    text: "Вы безвозвратно удалите этот приз!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Да, удалить!",
                    cancelButtonText: "Отменить!",
                    closeOnConfirm: false
                }, function () {
                    jQuery.get(btn.attr('href'));
                    btn.closest('tr').remove();
                    swal("Удален!", "Приз был удален.", "success");
                });
                return false;
            });
        });
    </script>
@endpush