@extends('layouts.app')

@section('content')
	<div class="ibox">
		<div class="ibox-title">
			<h5>Купленные призы</h5>
		</div>
		<div class="ibox-content">
            <div class="table-responsive">
                <table class="table">
                	<thead>
                        <tr>
                            <th width="150"> </th>
                            <th>Что купили </th>
                            <th>Кто купил </th>
                            <th>Управление</th>
                        </tr>
                    </thead>
                    <tbody>
						@foreach($buyed as $buy)
							<tr class="
								@if($buy->status == 'new')
									bg-yellow
								@elseif($buy->status == 'sended')
									bg-lgreen
								@elseif($buy->status == 'arrived')
									bg-green
								@elseif($buy->status == 'canceled')
									bg-red
								@endif
							">
		                        <td><img src="/img/contests/{{ $buy->photo }}" alt="" class="img-responsive"></td>
		                        <td><h4>{{ $buy->title }}</h4>@if($buy->created_at)<p>{{ \Date\DateFormat::post($buy->created_at) }}</p>@endif</td>
                                <td><h4><a href="{{ route('user', ['id' => $buy->user_id]) }}">{{ $buy->name }}</a></h4>
                                {{ $buy->agency }} | {{ $buy->score }} {{ trans_choice('балл|балла|баллов', $buy->score) }}</td>
		                        <td>
                                    @if( $buy->status != 'canceled')
                                        <select name="" id="" class="form-control" onchange="buyset(this)" data-id="{{ $buy->id }}" data-contest="{{ $buy->contest_id }}">
                                            <option value="new" @if($buy->status == 'new') selected="selected" @endif>Новый</option>
                                            <option value="sended" @if($buy->status == 'sended') selected="selected" @endif>Отправлен</option>
                                            <option value="arrived" @if($buy->status == 'arrived') selected="selected" @endif>Выдан</option>
                                            <option value="canceled" @if($buy->status == 'canceled') selected="selected" @endif>Отменен</option>
                                        </select>
                                    @endif      
                                </td>
		                    </tr>
						@endforeach()
                    </tbody>
                </table>
            </div>
		</div>
    </div>

    {{ $buyed->links() }}

@endsection

@push('js')
    <script>
        function buyset(e)
        {
            var status = jQuery(e).val();
            var id = jQuery(e).data('id');

            jQuery.post('/admin/contest/buyset', {status: status, id: id}, function(data){
                var classes = {'new': 'bg-yellow', 'sended': 'bg-lgreen', 'arrived': 'bg-green', 'canceled': 'bg-red'};
                if(data == 'false')
                {
                    jQuery(e).closest('tr').removeClass('bg-yellow').removeClass('bg-lgreen').removeClass('bg-green').removeClass('bg-red').removeClass('bg-muted').addClass('bg-muted');
                    swal("Нет доступа!", "Нет доступа у управлению выбранным заказом");
                }
                else
                {
                    jQuery(e).closest('tr').removeClass('bg-yellow').removeClass('bg-lgreen').removeClass('bg-green').removeClass('bg-red').removeClass('bg-muted').addClass(classes[status]);
                    swal("Статус изменен!", "", "success");
                }
            });
        }
    </script>
@endpush