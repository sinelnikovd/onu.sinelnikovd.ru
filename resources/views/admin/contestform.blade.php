@extends('layouts.app')

@section('content')

	<div class="ibox">
		<div class="ibox-title">
			<h5>Приз @if($contest) <span class="text-primary">{{ $contest->title }}</span> @endif</h5>
		</div>
		<div class="ibox-content">
            @include('partials.message')
            <form action="" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Название</label>
                    <input type="text" name="title" class="form-control" value="{{ old('title') ?: @$contest->title }}">
                </div>
                <div class="form-group">
                    <label>Описание</label>
                    <textarea name="content" id="" cols="30" rows="10" class="form-control">{{ old('content') ?: @$contest->content }}</textarea>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Цена</label>
                            <input type="number" name="score" class="form-control" value="{{ old('score') ?: @$contest->score }}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Активный?</label>
                            <input type="hidden" name="status" value="0">
                            <p><input type="checkbox" name="status" value="1" {{ (old('status') ?: @$contest->status) ? 'checked="checked"' : '' }}"></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Фото</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Выбрать</span>
                                <span class="fileinput-exists">Изменить</span>
                                    <input type="file" name="photo" accept="image/*" />
                                </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Отменить</a>
                            </div>
                            @if(@$contest->photo)
                                <p><img src="/img/contests/{{ $contest->photo }}" alt="" class="img-responsive"></p>
                            @endif
                        </div>
                    </div>
                </div>                
                <div class="form-group">
                    <input type="submit" value="Отправить" class="btn btn-primary">
                </div>
            </form>
		</div>
    </div>

@endsection