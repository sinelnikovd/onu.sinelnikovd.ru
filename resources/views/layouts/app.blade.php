<!DOCTYPE html>
<html>
{{ app('captcha')->multiple() }}
@if(auth()->check())
    <?php auth()->user()->update(['visited_at' => time()]) ?>
@endif
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="{{ config('app.url') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="wsserver" content="{{ env('WS_SERVER') }}">

    <title>{{ @$pagetitle ?: 'Страница не найдена' }} — {{ config('app.name') }}</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.css">

    <!-- Toastr style -->
    <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">
    
    <link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    
    <!-- Sweet Alert -->
    <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css?4" rel="stylesheet">

    <link href="css/plugins/slick/slick.css" rel="stylesheet">
    <link href="css/plugins/slick/slick-theme.css" rel="stylesheet">


    <link href="css/dropzone.css" rel="stylesheet">

    <!-- Cookie -->
    <script src="js/js.cookie.js"></script>

    <!-- Mainly scripts -->
    <!-- <script src="js/jquery-3.1.1.min.js"></script> -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js?1"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.js"></script>

    <!-- Socket.io -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
    @if(auth()->check())
    <script>
        var socket = io(':3000', {
            // origins: '{{ env('APP_URL') }}:*',
            query: 'id={{ auth()->id() }}@if(@$webinar)&webinarid={{ $webinar->id }}@endif'
        });
    </script>
    @endif

    <!-- slick carousel-->
    <script src="js/plugins/slick/slick.min.js"></script>

    <!-- Typehead -->
    <script src="js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="js/plugins/peity/jquery.peity.min.js"></script>
    <script src="js/demo/peity-demo.js"></script>
    
    <!-- Sweet alert -->
    <script src="js/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- DROPZONE -->
    <script src="js/plugins/dropzone/dropzone.js"></script>

    <script src="js/jQuery.paginate.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
    <script src="js/main.js?2"></script>

    <!-- jQuery UI -->
    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- Ladda -->
    <script src="js/plugins/ladda/spin.min.js"></script>
    <script src="js/plugins/ladda/ladda.min.js"></script>
    <script src="js/plugins/ladda/ladda.jquery.min.js"></script>

    <!-- Jasny -->
    <script src="js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- GITTER -->
    <script src="js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="js/demo/sparkline-demo.js"></script>

    <!-- Data picker -->
    <script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!-- Toastr -->
    <script src="js/plugins/toastr/toastr.min.js"></script>
    
    <script src="/js/jquery.form.min.js"></script>

    <link rel="stylesheet" type="text/css" href="/js/reformator/reformator.css" />
    
    @stack('js')

</head>

<body class="@if(@$_COOKIE['mini-sidebar']) mini-navbar @endif pace-done ">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu text-center" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <a href="/contests"><img alt="image" src="http://placehold.it/80x80"></a>
                             </span>
                        </div>
                        <div class="logo-element">
                            <img alt="image" src="http://placehold.it/40x40">
                        </div>
                    </li>
                    @if(auth()->check() && ($g_user_role == 'admin' || $g_user_role == 'superadmin'))
                        <li>
                            <a href="#"><i class="fa fa-2x fa-lock"></i><br><span class="nav-label">Админка</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li class="{{ request()->route() && request()->route()->getName() == 'admin.contests' ? 'active' : '' }}"><a href="{{ route('admin.contests') }}">Товары</a></li>
                            </ul>
                        </li>
                    @endif

                    @if(auth()->check())
                        <li class="{{ request()->route() && request()->route()->getName() == 'contests' ? 'active' : '' }}">
                            <a href="{{ route('contests') }}" title="Призы"><i class="fa fa-2x fa-star-o"></i><br><span class="nav-label">Призы</span></a>
                        </li>

                    @endif
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:;"><i class="fa fa-bars"></i> </a>
            </div>
                <ul class="nav navbar-top-links pull-right">				
					@if(auth()->check())
                        <li><a>{{ (integer)auth()->user()->score }} {{ trans_choice('рубль|рубля|рублей', (integer)auth()->user()->score) }}</a></li>
                        <li class="dropdown">
                            <a class="count-info" href="{{ route('wishlist') }}">
                                @php $g_wishlist_count = auth()->user()->wishlist()->count(); @endphp
                                <i class="fa fa-heart-o"></i>  @if((bool)$g_wishlist_count) <span class="label label-primary">{{ $g_wishlist_count }}</span> @endif
                            </a>
                        </li>
                    @endif
                    
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}"><i class="fa fa-sign-in"></i> Войти</a></li>
                    @else
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="{{ auth()->user()->avatar }}" alt="" class="nav-avatar img-circle circle-border avatar-{{ auth()->user()->id }}"> <span class="hidden-xs">{{ auth()->user()->name }}</span> <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out"></i> Выйти
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                          </ul>
                        </li>
                    @endif
                </ul>

            </nav>
        </div>
        @yield('header')

        <div class="wrapper wrapper-content animated fadeInDown">
            @yield('content')
        </div>

        </div>
    </div>
</body>
</html>
