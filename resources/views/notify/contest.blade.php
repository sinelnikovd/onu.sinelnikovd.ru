<div class="feed-activity-list">
    <div class="feed-element contest-notify">
        <span class="pull-left">
            <img alt="image" class="img-circle" src="/img/contests/{{ $contest->photo }}">
        </span>
        <div class="media-body ">
            <h4 class="m-b-xs">Приз {{ $contest->title }}</h4>
            <p class="m-b-xs" style="line-height: 20px;">Выбрал(а) <strong>{{ $user->name }}</strong><br><small class="text-muted">{{ $contest->created_at }}</small></p>
        </div>
    </div>
</div>