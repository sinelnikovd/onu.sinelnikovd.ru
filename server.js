const http = require('http');
var io = require('socket.io')(3000);
var users = {};
var Redis = require('ioredis'),
  redis = new Redis();

console.log('Server work');

io.use(function (socket, next) {
    var id = socket.handshake.query.id;
    var webinarid = socket.handshake.query.webinarid || false;
    if (id) {
    	var index = objectKeyByValue(users, id);
    	if(index)
    		delete users[index];
        users[socket.id] = id;
        return next();
    }
    next(new Error('Authentication error'));
});

redis.psubscribe('*');

redis.on('pmessage', function(pattern, channel, message) {
  message = JSON.parse(message);
  io.emit(channel+':'+message.event, message.data.message);
});

io.on('connection', function (socket) {
	io.emit('online:online', users);
  var id = socket.id;
  socket.on('disconnect', function () {
    http.get('/userlivewebinar/'+users[id]);
    delete users[id];
    io.emit('online:online', users);
  });
});

function objectKeyByValue (obj, val) {
  return Object.entries(obj).find(i => i[1] === val);
}