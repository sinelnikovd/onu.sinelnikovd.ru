jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
    }
});

function clear_notify(e)
{
	jQuery.get('/clearnotifies', function(){
		jQuery(e).closest('.dropdown-alerts').html('<li><div>Новых уведомлений нет</div></li>');
	});
}

jQuery(function(){
	Ladda.bind('.ladda-button');
	jQuery('[data-toggle="tooltip"]').tooltip();
	jQuery(".bbeditor").each(function(){
		jQuery(this).wysibb({
			buttons: "bold,italic,underline,strike,sup,sub,h2,|,bullist,numlist,|,img,video,link",
			lang: "ru",
			hotkeys: false,
				showHotkeys: false,
				imgupload: true,
				img_uploadurl: "/admin/upload_img",
				allButtons: {
					img : {
		          modal: {
		            tabs: [
		              {
		                title: CURLANG.modal_img_tab1,
		                input: [
		                  {param: "SRC",title:CURLANG.modal_imgsrc_text,validation: '^http(s)?://.*?\.(jpg|png|gif|jpeg)$'}
		                ]
		              },
		              { //The second tab
		                title: CURLANG.modal_img_tab2,
		                html: '<div id="imguploader"><form id="fupform" class="upload" action="{img_uploadurl}" method="post" enctype="multipart/form-data" target="fupload"><input type="hidden" name="_token" value="'+jQuery('meta[name="csrf-token"]').attr('content')+'"><input type="hidden" name="iframe" value="1"/><input type="hidden" name="idarea" value="' + this.id + '" /><div class="fileupload"><input id="fileupl" class="file" type="file" name="img" /><button id="nicebtn" class="wbb-button">' + CURLANG.modal_img_btn + '</button> </div> </form> </div><iframe id="fupload" name="fupload" src="about:blank" frameborder="0" style="width:0px;height:0px;display:none"></iframe></div>'
		              }
		            ],
		            onLoad: this.imgLoadModal,
		            onSubmit: function() {}
		          },
		          transform : {
		            '<img src="{SRC}" />':"[img]{SRC}[/img]"/*,
		             '<img src="{SRC}" ssswidth="{WIDTH}" height="{HEIGHT}"/>':"[img {WIDTH}x{HEIGHT}]{SRC}[/img]"*/
		          }
		        },
		        h2: {
				    title: 'Тег H2',
				    buttonText: 'H2',
				    transform: {
				    	'<h2>{SELTEXT}</h2>':'[h2]{SELTEXT}[/h2]'
				    }
			    }
			}
		});
	});
});

function ajax_form_init()
{
	jQuery('form.ajax').each(function(){
		var form = jQuery(this);
		form.ajaxForm({
	        success: function(data) {
				form.closest('.ibox-content').removeClass('sk-loading');
				if(data.error)
				{
					swal('Ошибка', data.message);
					return false;
				}
				location.href=data.message;
			},
			error: function(error) {
				form.closest('.ibox-content').removeClass('sk-loading');
				swal('Ошибка', error.responseText);
			},
			beforeSubmit: function() {
				form.closest('.ibox-content').addClass('sk-loading');
			}
		});
    });
}

function sendpost(e)
{
	var inp = jQuery('.chat-message-form').find('textarea');

	if(inp.val() == '') return false;

	jQuery.post(jQuery(e).data('url'), {text: inp.val()}, function(data){
		inp.val('');
	}).fail(function(data){
		console.log(data.responseText);
	});

	return false;
}

function friends(e)
{
	var btn = jQuery(e);

	var l = btn.ladda();
	l.ladda('start');

	jQuery.get('/api/friends/'+btn.data('id'), function(data){
		l.ladda('stop');
		if(data==1) {
			btn.removeClass('btn-primary').removeClass('btn-white').addClass('btn-white').text('Отменить дружбу');
		} else{
			btn.removeClass('btn-primary').removeClass('btn-white').addClass('btn-primary').text('Дружить');
		}
	});
}


function scrollbottom(e)
{
	e.scrollTop(e[0].scrollHeight);
}


/*function checknewpost() {
    setTimeout(function(){
        jQuery.get('/messenger/newcount', function(data){
            jQuery('.count-post span').remove();
            if(data > 0)
                jQuery('.count-post').append('<span class="label label-warning">'+data+'</span>');
        });
        checknewpost();
    }, 3000);
}*/

function mark(e)
{
	var score = jQuery(e).closest('.row').find('[name=score]').val();

	if(score == '')
	{
		swal('Оценка', 'Введите количество баллов', 'error');
		return false;
	}

	jQuery.post('/answer/'+jQuery(e).data('id'), {score: score}, function(data){
		jQuery(e).closest('.row').remove();
		swal('Оценка', 'Ответ оценен', 'success');
	});
}
